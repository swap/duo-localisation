## Description
Contains resources for localising UO project NB. Currently only British English is supported
This repository is intended to be used as submodule


## To update repository containing submodules:
If it's the first time you checkout a repo you need to use --init first:

```
git submodule update --init --recursive
```

From then on, call:

```
git submodule update --recursive --remote
```


## To push a change to a submodule

```
$ cd your_submodule
$ git checkout master
<hack,edit>
$ git commit -a -m "commit in submodule"
$ git push
$ cd ..
$ git add your_submodule
$ git commit -m "Updated submodule"
```


## Adding submodule to existing repository: 
If you want to incorporate this submodule in your repository, use this command
```
git submodule add https://gitlab.esss.lu.se/swap/duo-localisation <path_where_you_want_to_mount_submodule_in_your_repo>
```