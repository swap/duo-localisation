type KEY = string | number | symbol;

interface Dictionary<K extends KEY, V> {
  getKeys(): K[];
  getValues(): V[];
  get(key: K): V | null;
  put(key: K, val: V): void; // or boolean?
}

export class JSDict<K extends KEY, V> implements Dictionary<K, V> {
  private dict: { [key in K]?: V };

  constructor() {
    this.dict = {};
  }

  public getKeys() {
    let keys: K[] = [];
    for (let key in this.dict) {
      keys.push(key);
    }

    return keys;
  }

  public getValues() {
    let vals: V[] = [];
    for (let key in this.dict) {
      let v = this.dict[key];

      if (this.exists(v)) {
        vals.push(v);
      }
    }

    return vals;
  }

  // Type predicate to ensure v exists
  private exists(v: V | undefined): v is V {
    return v != null && typeof v !== "undefined";
  }

  public get(key: K) {
    let v = this.dict[key];

    return this.exists(v) ? v : null;
  }

  public put(key: K, val: V) {
    this.dict[key] = val;
  }

  static Create<Keys extends KEY, Values>() {
    return new JSDict<Keys, Values>();
  }
}

export type ResourceId =
  | "ACCOUNT_EXIST"
  | "BAD_TOKEN"
  | "COULD_NOT_FIND_USER_BY_EMAIL"
  | "COULD_NOT_VERIFY_USER"
  | "EMAIL_NOT_VERIFIED"
  | "INSUFFICIENT_PERMISSIONS"
  | "INTERNAL_ERROR"
  | "NO_ACTIVE_CALL_FOUND"
  | "NO_PROPOSER_ON_THE_PROPOSAL"
  | "NOT_ALLOWED_PROPOSAL_SUBMITTED"
  | "NOT_ALLOWED"
  | "NOT_AUTHORIZED"
  | "NOT_LOGGED_IN"
  | "NOT_LOGGED"
  | "NOT_REVIEWER_OF_PROPOSAL"
  | "NOT_USER_OFFICER"
  | "ORCID_HASH_MISMATCH"
  | "PROPOSAL_DOES_NOT_EXIST"
  | "TOO_SHORT_ABSTRACT"
  | "TOO_SHORT_NAME"
  | "TOO_SHORT_TITLE"
  | "USER_DOES_NOT_EXIST"
  | "VALUE_CONSTRAINT_REJECTION"
  | "WRONG_EMAIL_OR_PASSWORD";

class EnDictionary {
  map = JSDict.Create<ResourceId, string>();
  constructor() {
    this.map.put("ACCOUNT_EXIST", "Account already exists");
    this.map.put("BAD_TOKEN", "Bad token");
    this.map.put(
      "COULD_NOT_FIND_USER_BY_EMAIL",
      "Could not find user by email"
    );
    this.map.put("COULD_NOT_VERIFY_USER", "Could not verify user");
    this.map.put("EMAIL_NOT_VERIFIED", "Email not verified");
    this.map.put("INSUFFICIENT_PERMISSIONS", "Insufficient permissions");
    this.map.put("INTERNAL_ERROR", "Internal server error");
    this.map.put("NO_PROPOSER_ON_THE_PROPOSAL", "Proposal has no proposer");
    this.map.put(
      "NOT_ALLOWED_PROPOSAL_SUBMITTED",
      "The operation is not allowed, because the proposal is already submitted"
    );
    this.map.put("NOT_ALLOWED", "The operation is not allowed");
    this.map.put("NOT_AUTHORIZED", "Not authorized");
    this.map.put("NOT_LOGGED_IN", "Not logged in");
    this.map.put("NOT_LOGGED_IN", "Not logged in");
    this.map.put(
      "NOT_REVIEWER_OF_PROPOSAL",
      "You are not reviewer of the proposal"
    );
    this.map.put("NOT_USER_OFFICER", "Not a user officer");
    this.map.put("ORCID_HASH_MISMATCH", "ORCID hash mismatch");
    this.map.put("PROPOSAL_DOES_NOT_EXIST", "Proposal does not exist");
    this.map.put("TOO_SHORT_ABSTRACT", "The abstract is too short");
    this.map.put("TOO_SHORT_NAME", "The name is too short");
    this.map.put("TOO_SHORT_TITLE", "Title is too short");
    this.map.put("USER_DOES_NOT_EXIST", "User does not exist");
    this.map.put(
      "VALUE_CONSTRAINT_REJECTION",
      "Value does not match constraints"
    );
    this.map.put("WRONG_EMAIL_OR_PASSWORD", "Wrong email or password");
  }
}

const dictionary = new EnDictionary();

export function getTranslation(id: ResourceId): string {
  return dictionary.map.get(id) || id;
}
